<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\JsonResource;

class CatalogItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|JsonSerializable
     */

    public function toArray($request)
    {
        // calc the discout price based on the production date 
        $dayLeftBeforExpireDate = now()->diffInDays($this->produced_at->addDays(3));
        $discount = $dayLeftBeforExpireDate === 1 ? 20 : ($dayLeftBeforExpireDate === 0 ? 80 : 0);

        return [
            'pastry' => [
                'id'               => $this->pastry->id,
                'name'             => $this->pastry->name,
                'price'            => Str::replace('.', ',', $this->pastry->price),
                'discounted_price' => Str::replace('.', ',', $this->pastry->price - ($this->pastry->price * ($discount / 100))),
            ],
            'quantity'    => $this->quantity,
            'produced_at' => $this->produced_at->format('d/m/Y'),
            'expire_at'   => $this->produced_at->addDays(3)->format('d/m/Y'),
        ];
    }
}
