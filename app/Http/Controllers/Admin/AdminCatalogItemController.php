<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pastry;
use Illuminate\View\View;
use App\Models\CatalogItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\CatalogItemStoreRequest;

class AdminCatalogItemController extends Controller
{
    /**
     * Display a listing of the catalog items.
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.catalog-items.index', ['catalog_items' => CatalogItem::paginate(10)]);
    }

    /**
     * Show the form for creating a new catalog item.
     *
     * @return View
     */
    public function create(): View
    {
        $pastries = Pastry::all();
        
        return view('admin.catalog-items.create', compact('pastries'));
    }

    /**
     * Store a newly created catalog item in storage.
     *
     * @param CatalogItemStoreRequest $request
     * @return RedirectResponse
     */
    public function store(CatalogItemStoreRequest $request): RedirectResponse
    {
        if (CatalogItem::checkDuplicateByPastryAndDate($request->get('pastry_id'), $request->get('produced_at'))) {
            return redirect()->route('admin.catalog-items.index')->with('duplicate_error', 'Error: Duplicate catalog item! The pastry and production date already exist.');
        }

        CatalogItem::create($request->all());

        return redirect()->route('admin.catalog-items.index')->with('success', 'Catalog item successfully created.');
    }

    /**
     * Show the form for editing the specified catalog item.
     *
     * @param CatalogItem $catalogItem
     * @return View
     */
    public function edit(CatalogItem $catalogItem): View
    {
        $pastries = Pastry::all();

        return view('admin.catalog-items.edit', compact('catalogItem', 'pastries'));
    }

    /**
     * Update the specified catalog item in storage.
     *
     * @param CatalogItemStoreRequest $request
     * @param CatalogItem $catalogItem
     * @return RedirectResponse
     */
    public function update(CatalogItemStoreRequest $request, CatalogItem $catalogItem): RedirectResponse
    {
        if (CatalogItem::checkDuplicateByPastryAndDate($request->get('pastry_id'), $request->get('produced_at'))) {
            return redirect()->route('admin.catalog-items.index')->with('duplicate_error', 'Error: Duplicate catalog item! The pastry and production date already exist.');
        }

        $catalogItem->update($request->all());

        return redirect()->route('admin.catalog-items.index')->with('success', 'Catalog item successfully updated.');
    }

    /**
     * Remove the specified catalog item from storage.
     *
     * @param CatalogItem $catalogItem
     * @return RedirectResponse
     */
    public function destroy(CatalogItem $catalogItem): RedirectResponse
    {
        $catalogItem->delete();

        return redirect()->route('admin.catalog-items.index')->with('success', 'Catalog item successfully deleted.');
    }
}
