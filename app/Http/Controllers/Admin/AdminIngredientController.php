<?php

namespace App\Http\Controllers\Admin;

use Illuminate\View\View;
use App\Models\Ingredient;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\IngredientStoreRequest;

class AdminIngredientController extends Controller
{
    /**
     * Display a listing of the ingredients.
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.ingredients.index', ['ingredients' => Ingredient::paginate(10)]);
    }

    /**
     * Show the form for creating a new ingredient.
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.ingredients.create');
    }

    /**
     * Store a newly created ingrendient.
     *
     * @param IngredientStoreRequest $request
     * @return RedirectResponse.
     */
    public function store(IngredientStoreRequest $request): RedirectResponse
    {
        Ingredient::create($request->all());

        return redirect()->route('admin.ingredients.index')->with('success', 'Ingredient successfully created.');
    }

    /**
     * Show the form for editing the specified ingredient.
     *
     * @param Ingredient $ingredient
     * @return View
     */
    public function edit(Ingredient $ingredient): View
    {
        return view('admin.ingredients.edit', compact('ingredient'));
    }

    /**
     * Update the specified ingredient in storage.
     *
     * @param IngredientStoreRequest $request
     * @param Ingredient $ingredient
     * @return RedirectResponse
     */
    public function update(IngredientStoreRequest $request, Ingredient $ingredient): RedirectResponse
    {
        $ingredient->update($request->all());

        return redirect()->route('admin.ingredients.index')->with('success', 'Ingredient successfully updated.');
    }

    /**
     * Remove the specified ingredient from storage.
     *
     * @param Ingredient $ingredient
     * @return RedirectResponse
     */
    public function destroy(Ingredient $ingredient): RedirectResponse
    {
        $ingredient->delete();

        return redirect()->route('admin.ingredients.index')->with('success', 'Ingredient successfully deleted.');
    }
}
