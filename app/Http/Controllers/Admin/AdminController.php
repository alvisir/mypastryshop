<?php

namespace App\Http\Controllers\Admin;

use Illuminate\View\View;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display the main admin page.
     *
     * @return View
     */
    public function dashboard(): View
    {
        return view('admin.dashboard');
    }
}
