<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pastry;
use Illuminate\View\View;
use App\Models\Ingredient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\PastryStoreRequest;

class AdminPastryController extends Controller
{
    /**
     * Display a listing of the pastries.
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.pastries.index', ['pastries' => Pastry::paginate(10)]);
    }

    /**
     * Show the form for creating a new pastry.
     *
     * @return View
     */
    public function create(): View
    {
        $ingredients = array_column(Ingredient::all()->toArray(), null, 'id');

        return view('admin.pastries.create', compact('ingredients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PastryStoreRequest $request
     * @return RedirectResponse
     */
    public function store(PastryStoreRequest $request): RedirectResponse
    {
        $pastry = Pastry::create($request->only(['name', 'price']));
        if (
            ($ingredients = $request->input('ingredient')) &&
            ($quantities = $request->input('qty')) &&
            (count($ingredients) === count($quantities))
        ) {
            foreach ($ingredients as $index => $ingredientID) {
                $pastry->ingredients()->attach($ingredientID, ['quantity' => $quantities[$index]]);
            }
        }

        return redirect()->route('admin.pastries.index')->with('success', 'Pastry successfully created.');
    }

    /**
     * Show the form for editing the specified pastry.
     *
     * @param Pastry $pastry
     * @return View
     */
    public function edit(Pastry $pastry): View
    {
        $ingredients = array_column(Ingredient::all()->toArray(), null, 'id');

        return view('admin.pastries.edit', compact('pastry', 'ingredients'));
    }

    /**
     * Update the specified pastry in storage.
     *
     * @param PastryStoreRequest $request
     * @param Pastry $pastry
     * @return RedirectResponse
     */
    public function update(PastryStoreRequest $request, Pastry $pastry): RedirectResponse
    {
        $pastry->update($request->only(['name', 'price']));

        if (
            ($ingredients = $request->input('ingredient')) &&
            ($quantities = $request->input('qty')) &&
            (count($ingredients) === count($quantities))
        ) {
            $syncData = [];
            foreach ($ingredients as $index => $ingredientID) {
                $syncData[$ingredientID] = ['quantity' => $quantities[$index]];
            }
            $pastry->ingredients()->sync($syncData);
        }

        return redirect()->route('admin.pastries.index')->with('success', 'Pastry successfully updated.');
    }

    /**
     * Remove the specified pastry from storage.
     *
     * @param Pastry $pastry
     * @return RedirectResponse
     */
    public function destroy(Pastry $pastry): RedirectResponse
    {
        $pastry->delete();

        return redirect()->route('admin.pastries.index')->with('success', 'Pastry successfully deleted.');
    }
}
