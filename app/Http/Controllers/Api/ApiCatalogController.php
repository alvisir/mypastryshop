<?php

namespace App\Http\Controllers\Api;

use App\Models\CatalogItem;
use App\Http\Controllers\Controller;
use App\Http\Resources\CatalogItemCollection;
use App\Http\Resources\IngredientCollection;
use App\Models\Pastry;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ApiCatalogController extends Controller
{
    /**
     * Display catalog items list
     * Obtain items not yet expired and not with a future production date.
     *
     * @return ResourceCollection
     */
    public function items(): ResourceCollection
    {
        $validCatalogItems = CatalogItem::whereDate('produced_at', '<=', now())
                                ->whereDate('produced_at', '>', now()->subDays(3))
                                ->where('quantity', '>', 0)
                                ->get();

        return new CatalogItemCollection($validCatalogItems);
    }

    /**
     * Display ingredients of specific pastry
     *
     * @param Pastry $pastry
     * @return ResourceCollection
     */
    public function pastryIngredients(Pastry $pastry): ResourceCollection
    {
        return new IngredientCollection($pastry->ingredients);
    }
}
