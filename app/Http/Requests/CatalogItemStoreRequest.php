<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CatalogItemStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'pastry_id'   => 'required|exists:pastries,id',
            'quantity'    => 'required|integer|gt:0',
            'produced_at' => 'required|date',
        ];
    }
}
