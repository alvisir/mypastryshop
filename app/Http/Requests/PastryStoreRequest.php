<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PastryStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'         => 'required|string|max:256',
            'price'        => 'required|numeric|gt:0',
            'ingredient.*' => 'exists:ingredients,id',
            'qty.*'        => 'numeric|gt:0',
        ];
    }
}
