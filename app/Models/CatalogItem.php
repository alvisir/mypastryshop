<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CatalogItem extends Model
{
    use HasFactory;

    /**
     * @var array<int, string>
     */
    protected $fillable = [
        'pastry_id',
        'produced_at',
        'quantity',
    ];

    /**
     * @var array<int, string>
     */
    protected $with = [
        'pastry'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'produced_at' => 'date:d-m-Y',
    ];

    /**
     * @return BelongsTo
     */
    public function pastry(): BelongsTo
    {
        return $this->belongsTo(Pastry::class);
    }

    /**
     * Check if a catalog item with same pastry and production date exists.
     *
     * @param integer $pastryId
     * @param string $productionDate
     * @return boolean
     */
    public static function checkDuplicateByPastryAndDate(int $pastryId, string $productionDate): bool
    {
        return self::where('pastry_id', $pastryId)->whereDate('produced_at', $productionDate)->exists();
    }
}
