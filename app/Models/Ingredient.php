<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Ingredient extends Model
{
    use HasFactory;
    
    /**
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'unit_of_measurement',
    ];

    /**
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return BelongsToMany
     */
    public function pastries(): BelongsToMany
    {
        return $this->belongsToMany(Pastry::class);
    }
}
