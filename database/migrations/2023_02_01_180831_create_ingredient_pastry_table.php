<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_pastry', function (Blueprint $table) {
            $table->foreignId('pastry_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('ingredient_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->float('quantity', 8, 2);
            $table->unique(['pastry_id', 'ingredient_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_pastry');
    }
};
