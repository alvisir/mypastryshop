<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredients = [
           ['name' => 'Flour', 'unit_of_measurement' => 'gr'], 
           ['name' => 'Water', 'unit_of_measurement' => 'ml'], 
           ['name' => 'Milk', 'unit_of_measurement' => 'ml'],
           ['name' => 'Cocoa', 'unit_of_measurement' => 'gr'],
           ['name' => 'Sugar', 'unit_of_measurement' => 'gr'],
           ['name' => 'Salt', 'unit_of_measurement' => 'gr'],
           ['name' => 'Yeast', 'unit_of_measurement' => 'gr'],
           ['name' => 'Eggs', 'unit_of_measurement' => null],
           ['name' => 'Apples', 'unit_of_measurement' => null],
           ['name' => 'Cream', 'unit_of_measurement' => 'ml'],
           ['name' => 'Coconut Flour', 'unit_of_measurement' => 'gr'],
           ['name' => 'Butter', 'unit_of_measurement' => 'gr'],
           ['name' => 'White Chocolate', 'unit_of_measurement' => 'gr'],
        ];

        Ingredient::upsert($ingredients, ['id']);
    }
}
