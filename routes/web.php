<?php

use App\Http\Controllers\Admin\AdminCatalogItemController;
use App\Http\Controllers\Admin\AdminIngredientController;
use App\Http\Controllers\Admin\AdminPastryController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Public routes
 */
Route::get('/', fn() => view('index'))->name('index');

/**
 * Admin routes
 */
Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {

    Route::get('/', fn() => redirect()->route('admin.dashboard'));

    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');

    Route::prefix('profile')->name('profile.')->group(function(){
        Route::get('/', [AdminProfileController::class, 'edit'])->name('edit');
        Route::patch('/', [AdminProfileController::class, 'update'])->name('update');
        Route::delete('/', [AdminProfileController::class, 'destroy'])->name('destroy');
    });

    Route::resource('pastries', AdminPastryController::class, ['except'=> ['show']]);
    Route::resource('ingredients', AdminIngredientController::class, ['except'=> ['show']]);
    Route::resource('catalog-items', AdminCatalogItemController::class, ['except'=> ['show']]);
});

/**
 * Auth routes
 */
require __DIR__.'/auth.php';
