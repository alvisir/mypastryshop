# MyPastryShop

A simple web app for pastry shops to manage pastries, ingredients and related catalog.

Back-end: **Laravel 9**<br>
Front-end: **Alpine.js**

  ## Installation

Before install make sure you have all system requirements to run <strong>Laravel 9</strong> (<a  target="_blank"  href="https://laravel.com/docs/9.x/deployment">See the official documentation</a>). It's recommended to use MariaDB as database.

- Clone this repo in your environment
- In root directory copy `.env.example` file and rename it to `.env`
- Open your `.env` file and set the following variables according to your environment configuration
    
    - `APP_URL`
    - `DB_HOST`
    - `DB_PORT`
    - `DB_DATABASE`
    - `DB_USERNAME`
    - `DB_PASSWORD`
    - `MAIL_HOST/MAIL_PORT`
    - `APP_NAME` (Optional. I suggest **MyPastryShop**)
    - `VITE_API_URL_ENDPOINT="${APP_URL}/api"` (don't change it!)
 - In root directory run these commands in order:
    - `composer install`
    - `php artisan key:generate`
    - `php artisan migrate`
    - `php artisan db:seed`
    - `php artisan serve`
    - `npm install`
    - `npm run dev` or `npm run build`
 
 If everything is ok you should be able to access your app via the app url you set earlier (eg. `http://localhost:8000`).
 A user will be created automatically during installation
    
    username: info@mypastryshop.test
    password: mypastryshop

## Usage
This app consists of 2 sections:
- an admin panel where you can manage ingredients, pastries, catalog and user profile.<br>
Path: `/admin`
- a simple homepage where the catalog of products for sale will be displayed including the authentication forms.
