<x-guest-layout>
    <div x-data="catalogData">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Our pastries catalog') }}
        </h2>
        <template x-if="loadingItems"><div class="mt-4 text-gray-400 text-center">{{ __('Loading items...') }}</div></template>
        <template x-if="!loadingItems && !catalog.length"><div class="mt-4 text-gray-400 text-center">{{ __('At the moment there are no pastries in our catalog.') }}</div></template>
        <template x-if="catalog.length">
            <div>
                <p class="text-xs my-4">{{ __('Click on element to show ingredients.') }}</p>
                <div class="space-y-4">
                    <template x-for="(item, index) in catalog" :key="index">
                        <div 
                            @click="loadIngredients(item.pastry.id, index)"
                            :class="showIngredientsByItem === index ? 'bg-gray-100' : ''"
                            class="group p-8 border border-gray-100 rounded-md cursor-pointer shadow-md hover:bg-gray-100 transition ease-in-out"
                        >
                            <div class="flex flex-row items-center mb-2">
                                <h3 class="text-xl font-bold text-pink-600" x-text="item.pastry.name"></h3>
                                <div class="text-right ml-auto">
                                    <template x-if="item.pastry.price != item.pastry.discounted_price">
                                        <span class="mr-2 font-bold" x-text="item.pastry.discounted_price + ' €'"></span>
                                    </template>
                                    <span :class="item.pastry.price != item.pastry.discounted_price ? 'line-through text-gray-500 font-medium' : 'font-bold'" class="ml-auto" x-text="item.pastry.price + ' €'"></span><span class="text-xs ml-2">{{ __('/ piece') }}</span>
                                </div>
                            </div>
                            <div class="flex sm:flex-row flex-col sm:items-center">
                                <div class="sm:basis-1/3 basis-full"><span class="font-semibold" x-text="item.quantity"></span> {{ __('pieces') }}</div>
                                <div class="sm:basis-2/3 basis-full text-xs sm:text-right mt-3 sm:mt-0 text-gray-600">
                                    <span class="mr-3">{{ __('Produced at:') }} <span class="font-semibold" x-text="item.produced_at"></span></span>
                                    <span>{{ __('Expire at:') }} <span class="font-semibold text-red-600" x-text="item.expire_at"></span></span>
                                </div>
                            </div>
                            <template x-if="loadingIngredientsByItem === index"><div class="mt-4 text-gray-400 text-center">{{ __('Loading ingredients...') }}</div></template>
                            <template x-if="showIngredientsByItem === index">
                                <div class="flex sm:flex-row flex-col mt-4 space-y-2 p-4 bg-gray-100 group-[.bg-gray-100]:bg-white">
                                    <h4 class="sm:basis-1/3 basis-full font-bold">{{ __('Ingredients') }}</h4>
                                    <div class="sm:basis-2/3 basis-full">
                                        <template x-for="ingredient in pastryIngredients[item.pastry.id]" :key="ingredient.id">
                                            <div class="flex flex-row text-sm ">
                                                <h5 class="font-bold basis-1/2 text-pink-600" x-text="ingredient.name"></h5>
                                                <span class="basis-1/2" x-text="ingredient.qty"></span>
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </template>
                </div>
            </div>
        </template>
    </div>
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('catalogData', () => ({
                catalog: [],
                pastryIngredients: [],
                showIngredientsByItem: false,
                loadingIngredientsByItem: false,
                loadingItems: true,
                async init() {
                    await (await fetch(Alpine.store('apiUrl')+'/catalog-items').then(response => response.json()).then(data => this.catalog = data.data));
                    this.loadingItems = false;
                },
                async loadIngredients(pastryId, indexItem) {
                    if (this.showIngredientsByItem === indexItem) {
                        this.showIngredientsByItem = false;
                        return;
                    }

                    if (!this.pastryIngredients.hasOwnProperty(pastryId)) {
                        this.loadingIngredientsByItem = indexItem;
                        await (await fetch(Alpine.store('apiUrl')+'/pastries/'+pastryId+'/ingredients').then(response => response.json()).then(data => this.pastryIngredients[pastryId] = data.data));
                        this.loadingIngredientsByItem = false;
                    }

                    this.showIngredientsByItem = indexItem;
                }
            }))
        })
    </script>
</x-guest-layout>
