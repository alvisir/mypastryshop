<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="font-sans text-gray-900 antialiased">
        <div class="p-2 text-right align-middle text-sm font-semibold">
            @if (!auth()->check())
                @if (Route::currentRouteName() != 'login')
                <a class="inline-block mx-2" href="{{ route('login') }}">{{ __('Login') }}</a>
                @endif
                @if (Route::currentRouteName() != 'register')
                <a class="inline-block mx-2" href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                @endif
            @else
                <a class="inline-block mx-2" href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a>
            @endif
        </div>
        <div class="min-h-screen flex flex-col items-center p-6 bg-gray-100">
            <div>
                <a class="block text-center" href="/">
                    <x-application-logo class="w-32 h-32 fill-current" />
                    <h1 class="mt-3 font-bold text-pink-600 text-2xl">{{ config('app.name') }}</h1>
                </a>
            </div>
            @if (Route::currentRouteName() == 'index')
            <div class="w-full sm:max-w-4xl mt-8 p-8 bg-white shadow-md overflow-hidden sm:rounded-lg">
            @else
            <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
            @endif
                {{ $slot }}
            </div>
        </div>
    </body>
</html>
