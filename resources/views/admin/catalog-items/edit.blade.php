<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <small class="block mb-2">{{ __('Edit catalog item:') }}</small>
            <span class="text-gray-500">{{ $catalogItem->pastry->name }} - {{ $catalogItem->produced_at->format('d/m/Y') }}</span>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <section>
                    <x-error-message :hasErrors="(bool) $errors->count()">{{ __('Catalog item editing failed!') }}</x-error-message>
                    <form method="post" action="{{ route('admin.catalog-items.update', $catalogItem->id) }}" class="space-y-6 space-y-reverse">
                        @csrf
                        @method('patch')

                        <div class="flex flex-row space-x-4">
                            <div class="basis-1/4">
                                <x-input-label for="pastry" :value="__('Pastry')" />
                                <select id="pastry" name="pastry_id" class="border border-gray-300 text-gray-900 rounded-md shadow-sm block w-full mt-1" required>
                                    <option value="">{{ __('Select a pastry') }}</option>
                                    @foreach($pastries as $pastry)
                                    <option value="{{ $pastry->id }}" @if(old('pastry_id', $catalogItem->pastry->id) == $pastry->id) selected @endif">{{ $pastry->name }}</option>
                                    @endforeach
                                </select>
                                <x-input-error class="mt-2" :messages="$errors->get('pastry_id')" />
                            </div>

                            <div class="basis-1/4">
                                <x-input-label for="produced-at" :value="__('Produced at')" />
                                <x-text-input id="produced-at" name="produced_at" type="date" class="mt-1 block w-full" :value="old('produced_at', $catalogItem->produced_at->format('Y-m-d'))" required />
                                <x-input-error class="mt-2" :messages="$errors->get('produced_at')" />
                            </div>

                            <div class="basis-1/4">
                                <x-input-label for="quantity" :value="__('Quantity')" />
                                <x-text-input id="quantity" name="quantity" type="number" step="1" min="1" class="mt-1 block w-[100px]" :value="old('quantity', $catalogItem->quantity)" required />
                                <x-input-error class="mt-2" :messages="$errors->get('quantity')" />
                            </div>
                        </div>
                        <x-input-description>{{ __('Note: If you create a catalog item with a pastry and production date already created, they will be aggregated.') }}</x-input-description>

                        <hr>
                        
                        <div class="flex items-center gap-4">        
                            <x-primary-button>{{ __('Save') }}</x-primary-button>
                            <x-button-link color="light-gray" :href="route('admin.catalog-items.index')">{{ __('< Back') }}</x-button-link>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</x-app-layout>