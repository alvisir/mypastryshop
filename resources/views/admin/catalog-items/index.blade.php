<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Catalog') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <x-succcess-message :success="Session::get('success')">{{ Session::get('success') }}</x-succcess-message>
                    <x-error-message :hasErrors="Session::get('duplicate_error')">{{ Session::get('duplicate_error') }}</x-error-message>
                    <x-button-link :href="route('admin.catalog-items.create')">{{ __('Add new') }}</x-button-link>
                    @if ($catalog_items->count())
                    <table class="table-auto w-full mt-6">
                        <thead class="text-left bg-gray-100">
                            <tr>
                                <th class="p-3">{{ __('Pastry') }}</th>
                                <th class="p-3">{{ __('Quantity') }}</th>
                                <th class="p-3">{{ __('Production date') }}</th>
                                <th class="p-3">{{ __('Expiry date') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($catalog_items as $item)
                            <tr class="hover:bg-gray-200">
                                <td class="p-3 border-t border-gray-100 font-semibold">{{ $item->pastry->name }}</td>
                                <td class="p-3 border-t border-gray-100 font-semibold"><span class="inline-block py-1 px-2 bg-gray-100 rounded-xl">{{ $item->quantity }}</span></td>
                                <td class="p-3 border-t border-gray-100 font-semibold">{{ $item->produced_at->format('d-m-Y') }}</td>
                                <td class="p-3 border-t border-gray-100 font-semibold"><span class="text-red-600 font-bold">{{ $item->produced_at->addDays(3)->format('d-m-Y') }}</span></td>
                                <td class="p-3 border-t border-gray-100 text-right">
                                    <x-button-link :href="route('admin.catalog-items.edit', $item->id)" color="blue" size="small">{{ __('Edit') }}</x-button-link>
                                    <form x-data="{}" @submit.prevent x-ref="deleteForm{{ $item->id }}" action="{{ route('admin.catalog-items.destroy', $item->id) }}" method="post" class="inline-block">
                                        @csrf
                                        @method('delete')
                                        <x-danger-button @click="if (confirm('{{ __('Confirm to delete') }}')) $refs.deleteForm{{$item->id}}.submit();" class="px-2 py-1 font-bold ml-3">{{ __('Delete') }}</x-danger-button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr>
                    <div class="mt-8">
                        {{ $catalog_items->links() }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>