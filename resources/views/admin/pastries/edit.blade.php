<x-app-layout>
    <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <small class="block mb-2">{{ __('Edit pastry:') }}</small>
            <span class="text-gray-500">{{ $pastry->name }}</span>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div class="max-w-xl">
                    <section>
                        <x-error-message :hasErrors="(bool) $errors->count()">{{ __('Pastry update failed!') }}</x-error-message>
                        <form method="post" action="{{ route('admin.pastries.update', $pastry->id) }}" class="space-y-6 space-y-reverse">
                            @csrf
                            @method('patch')

                            <div>
                                <x-input-label for="name" :value="__('Name')" />
                                <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $pastry->name)" required />
                                <x-input-description>{{ __('Max 256 characters') }}</x-input-description>
                                <x-input-error class="mt-2" :messages="$errors->get('name')" />
                            </div>

                            <div>
                                <x-input-label for="price" :value="__('Price')" />
                                <x-text-input id="price" name="price" type="number" step="0.01" class="mt-1" :value="old('price', $pastry->price)" required /><span class="ml-2">€</span>
                                <x-input-description>{{ __('You can insert amount with dot or comma') }}</x-input-description>
                                <x-input-error class="mt-2" :messages="$errors->get('price')" />
                            </div>
                            
                            <h3>{{ __('Ingredients') }}</h3>

                            @php
                            $ingredients_pivot_data = array_column($pastry->ingredients->toArray(), 'pivot');
                            $old_ingredients = array_column($ingredients_pivot_data, 'ingredient_id');
                            $old_qtys = array_column($ingredients_pivot_data, 'quantity');
                            @endphp

                            <div>
                                <x-ingredients-field :ingredients=$ingredients :old_ingredients="old('ingredient', $old_ingredients)" :old_qty="old('qty', $old_qtys)"></x-ingredients-field>
                            </div>

                            <hr>

                            <div class="flex items-center gap-4">
                                <x-primary-button>{{ __('Save') }}</x-primary-button>
                                <x-button-link color="light-gray" :href="route('admin.pastries.index')">{{ __('< Back') }}</x-button-link>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>