<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add New Pastry') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div class="max-w-xl">
                    <section>
                        <x-error-message :hasErrors="(bool) $errors->count()">{{ __('Pastry creation failed!') }}</x-error-message>
                        <form method="post" action="{{ route('admin.pastries.store') }}" class="space-y-6 space-y-reverse">
                            @csrf

                            <div>
                                <x-input-label for="name" :value="__('Name')" />
                                <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', '')" required />
                                <x-input-description>{{ __('Max 256 characters') }}</x-input-description>
                                <x-input-error class="mt-2" :messages="$errors->get('name')" />
                            </div>

                            <div>
                                <x-input-label for="price" :value="__('Price')" />
                                <x-text-input id="price" name="price" type="number" step="0.01" class="mt-1" :value="old('price', '')" required /><span class="ml-2">€</span>
                                <x-input-description>{{ __('You can insert amount with dot or comma') }}</x-input-description>
                                <x-input-error class="mt-2" :messages="$errors->get('price')" />
                            </div>
                            
                            <h3>{{ __('Ingredients') }}</h3>

                            <div>
                                <x-ingredients-field :ingredients=$ingredients :old_ingredients="old('ingredient', [])" :old_qty="old('qty', [])"></x-ingredients-field>
                            </div>

                            <hr>

                            <div class="flex items-center gap-4">
                                <x-primary-button>{{ __('Save') }}</x-primary-button>
                                <x-button-link color="light-gray" :href="route('admin.pastries.index')">{{ __('< Back') }}</x-button-link>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>