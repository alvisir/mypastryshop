<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Pastries List') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <x-succcess-message :success="Session::get('success')">{{ Session::get('success') }}</x-succcess-message>
                    <x-button-link :href="route('admin.pastries.create')">{{ __('Add new') }}</x-button-link>
                    @if ($pastries->count())
                    <table class="table-auto w-full mt-6">
                        <thead class="text-left bg-gray-100">
                            <tr>
                                <th class="p-3">{{ __('Name') }}</th>
                                <th class="p-3">{{ __('Price') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pastries as $pastry)
                            <tr class="hover:bg-gray-200">
                                <td class="p-3 border-t border-gray-100 font-semibold">{{ $pastry['name'] }}</td>
                                <td class="p-3 border-t border-gray-100">{{ Str::replace('.', ',', $pastry['price']) }} €</td>
                                <td class="p-3 border-t border-gray-100 text-right">
                                    <x-button-link :href="route('admin.pastries.edit', $pastry['id'])" color="blue" size="small">{{ __('Edit') }}</x-button-link>
                                    <form x-data="{}" @submit.prevent x-ref="deleteForm{{$pastry['id']}}" action="{{ route('admin.pastries.destroy', $pastry['id']) }}" method="post" class="inline-block">
                                        @csrf
                                        @method('delete')
                                        <x-danger-button @click="if (confirm('{{ __('Confirm to delete') }}')) $refs.deleteForm{{$pastry['id']}}.submit();" class="px-2 py-1 font-bold ml-3">{{ __('Delete') }}</x-danger-button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr>
                    <div class="mt-8">
                        {{ $pastries->links() }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>