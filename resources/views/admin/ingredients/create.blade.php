<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add New Ingredient') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div class="max-w-xl">
                    <section>
                        <x-error-message :hasErrors="(bool) $errors->count()">{{ __('Ingredient creation failed!') }}</x-error-message>
                        <form method="post" action="{{ route('admin.ingredients.store') }}" class="space-y-6 space-y-reverse">
                            @csrf

                            <div>
                                <x-input-label for="name" :value="__('Name')" />
                                <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', '')" required />
                                <x-input-description>{{ __('Max 256 characters') }}</x-input-description>
                                <x-input-error class="mt-2" :messages="$errors->get('name')" />
                            </div>

                            <div>
                                <x-input-label for="unit-of-measurement" :value="__('Unit of measurement')" />
                                <select id="unit-of-measurement" name="unit_of_measurement" class="border border-gray-300 text-gray-900 rounded-md shadow-sm block flex-none mt-3">
                                    <option value="" @selected(old('unit_of_measurement', '') == '')>{{ __('None') }}</option>
                                    <option value="gr" @selected(old('unit_of_measurement', '') == '')>{{ __('Grams (gr)') }}</option>
                                    <option value="ml" @selected(old('unit_of_measurement', '') == '')>{{ __('Milliliters (ml)') }}</option>
                                </select>
                                <x-input-error class="mt-2" :messages="$errors->get('unit_of_measurement')" />
                            </div>

                            <hr>

                            <div class="flex items-center gap-4">        
                                <x-primary-button>{{ __('Save') }}</x-primary-button>
                                <x-button-link color="light-gray" :href="route('admin.ingredients.index')">{{ __('< Back') }}</x-button-link>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>