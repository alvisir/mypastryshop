<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <ul class="grid grid-cols-2 gap-6 text-center">
                        <li>
                            <a class="block py-8 px-12 border border-gray-200 rounded-lg font-semibold text-xl hover:bg-gray-100" href="{{ route('admin.pastries.index') }}">{{ __('Pastries management') }}</a>
                        </li>
                        <li>
                            <a class="block py-8 px-12 border border-gray-200 rounded-lg font-semibold text-xl hover:bg-gray-100" href="{{ route('admin.ingredients.index') }}">{{ __('Ingredients management') }}</a>
                        </li>
                        <li>
                            <a class="block py-8 px-12 border border-gray-200 rounded-lg font-semibold text-xl hover:bg-gray-100" href="{{ route('admin.catalog-items.index') }}">{{ __('Catalog management') }}</a>
                        </li>
                        <li>
                            <a class="block py-8 px-12 border border-gray-200 rounded-lg font-semibold text-xl hover:bg-gray-100" href="{{ route('admin.profile.edit') }}">{{ __('User profile') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
