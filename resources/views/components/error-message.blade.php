@props(['hasErrors'])

@if ($hasErrors)
    <div class="mb-6 px-6 py-3 border border-red-300 bg-red-200 text-red-700 font-semibold rounded-md">{{ $slot }}</div>
@endif