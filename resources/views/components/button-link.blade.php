@props(['color' => 'green', 'size' => 'big'])

@php
$classes = [
  'green' => [
    'big'   => 'px-4 py-2 bg-green-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-green-500 focus:bg-green-500 active:bg-green-700 focus:outline-none transition ease-in-out duration-150',
    'small' => 'px-2 py-1 bg-green-600 border border-transparent rounded-md font-bold text-xs text-white uppercase tracking-widest hover:bg-green-500 focus:bg-green-500 active:bg-green-700 focus:outline-none transition ease-in-out duration-150',
  ],
  'blue' => [
    'big'   => 'px-4 py-2 bg-sky-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-sky-500 focus:bg-sky-500 active:bg-sky-700 focus:outline-none transition ease-in-out duration-150',
    'small' => 'px-2 py-1 bg-sky-600 border border-transparent rounded-md font-bold text-xs text-white uppercase tracking-widest hover:bg-sky-500 focus:bg-sky-500 active:bg-sky-700 focus:outline-none transition ease-in-out duration-150',
  ],
  'red' => [
    'big'   => 'px-4 py-2 bg-red-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-500 focus:bg-red-500 active:bg-red-700 focus:outline-none transition ease-in-out duration-150',
    'small' => 'px-2 py-1 bg-red-600 border border-transparent rounded-md font-bold text-xs text-white uppercase tracking-widest hover:bg-red-500 focus:bg-red-500 active:bg-red-700 focus:outline-none transition ease-in-out duration-150',
  ],
  'orange' => [
    'big'   => 'px-4 py-2 bg-orange-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-500 focus:bg-orange-500 active:bg-orange-700 focus:outline-none transition ease-in-out duration-150',
    'small' => 'px-2 py-1 bg-orange-600 border border-transparent rounded-md font-bold text-xs text-white uppercase tracking-widest hover:bg-orange-500 focus:bg-orange-500 active:bg-orange-700 focus:outline-none transition ease-in-out duration-150',
  ],
  'gray' => [
    'big'   => 'px-4 py-2 bg-gray-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-500 focus:bg-gray-500 active:bg-gray-700 focus:outline-none transition ease-in-out duration-150',
    'small' => 'px-2 py-1 bg-gray-600 border border-transparent rounded-md font-bold text-xs text-white uppercase tracking-widest hover:bg-gray-500 focus:bg-gray-500 active:bg-gray-700 focus:outline-none transition ease-in-out duration-150',
  ],
  'light-gray' => [
    'big'   => 'px-4 py-2 bg-gray-100 border border-transparent rounded-md font-semibold text-xs uppercase tracking-widest hover:bg-gray-200 focus:bg-gray-200 active:bg-gray-700 focus:outline-none transition ease-in-out duration-150',
    'small' => 'px-2 py-1 bg-gray-100 border border-transparent rounded-md font-bold text-xs uppercase tracking-widest hover:bg-gray-200 focus:bg-gray-200 active:bg-gray-700 focus:outline-none transition ease-in-out duration-150',
  ],
  
];
@endphp

<a {{ $attributes->merge(['class' => $classes[$color][$size]]) }}>
  {{ $slot }}
</a>