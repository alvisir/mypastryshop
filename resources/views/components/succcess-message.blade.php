@props(['success'])

@if ($success)
    <div class="flex flex-row mb-6 px-6 py-3 border border-green-200 bg-green-100 text-green-700 font-semibold rounded-md">
        {{ $slot }}
        <span class="font-bold ml-auto cursor-pointer inline-block" x-init @click="event.target.parentElement.remove();">x</span>
    </div>
@endif