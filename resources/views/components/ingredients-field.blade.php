@props(['ingredients' => [], 'old_ingredients' => [], 'old_qty' => []])

<div x-data="tableIngredientsHandler()">
    <div class="flex w-full">
        <select x-model="selectedIngredient" class="border border-gray-300 text-gray-900 rounded-md shadow-sm block flex-none mr-3">
            <option disabled value="">{{ __('Select an ingredient') }}</option>
        @foreach($ingredients as $id => $ingredient)
            <option x-bind:disabled="selectedIngredients.includes({{ $id }})" value="{{ json_encode(['id' => $id, 'name' => $ingredient['name'], 'unit_of_measurement' => $ingredient['unit_of_measurement']]) }}">{{ $ingredient['name']}}</option>
        @endforeach
        </select>
        <x-secondary-button class="flex-none" @click="addIngredient(selectedIngredient)">{{ __('+ Add Ingredient') }}</x-secondary-button>
    </div>

    @if ($errors->get('qty.*'))
    <div class="text-sm text-red-600 my-6 font-bold">{{ __('Incorrect quantity values!') }}</div>
    @endif
    
    <table class="table-auto w-full mt-6 text-left" x-show="showIngredients">
        <thead class="bg-gray-100">
            <tr>
                <th class="p-3">{{ __('Ingredient') }}</th>
                <th class="p-3">{{ __('Quantity') }}</th>
                <th class="p-3"></th>
            </tr>
        </thead>
        <tbody>
            <template x-for="(item, index) in ingredientsList" :key="index">
                <tr>
                    <td class="p-3 border-t border-gray-100">
                        <input type="hidden" name="ingredient[]" x-bind:value="item.ingredient.id"> 
                        <div x-text="item.ingredient.name"></div>
                    </td>
                    <td class="p-3 border-t border-gray-100">
                        <x-text-input name="qty[]" type="number" step="0.01" min="0.01" class="w-[100px]" x-bind:value="item.qty"></x-text-input>
                        <span class="ml-2" x-text="item.ingredient.unit_of_measurement"></span>
                    </td>
                    <td class="p-3 border-t border-gray-100 text-right">
                        <x-button-link href="#" color="red" size="small" @click.prevent="removeIngredient(index)">&times;</x-button-link>
                    </td>
                </tr>
            </template>
        </tbody>
    </table>
</div>

<script>
    const allIngredients = {!! json_encode($ingredients) !!};
    const old_ingredients = {!! json_encode($old_ingredients) !!};
    const old_qty = {!! json_encode($old_qty) !!};
    const ingredients = [];
    old_ingredients.forEach((ingrId, index) => {
        ingredients.push({
            ingredient: allIngredients[parseInt(ingrId)],
            qty: old_qty[parseInt(index)],
        });
    });

    const tableIngredientsHandler = () => ({
            selectedIngredient: '',
            ingredientsList: ingredients,
            selectedIngredients: ingredients.map(item => item.ingredient.id),
            showIngredients: Object.keys(ingredients).length > 0,
            addIngredient: function (ingredient) {
                if (!ingredient) {
                    return;
                }
                ingredient = JSON.parse(ingredient);
                this.ingredientsList.push({
                    ingredient: ingredient,
                    qty: ''
                });
                this.showIngredients = true;
                this.selectedIngredients.push(ingredient.id);
            },
            removeIngredient: function(index) {
                this.ingredientsList.splice(index, 1);
                this.selectedIngredients.splice(index, 1);
                if (Object.keys(this.selectedIngredients).length === 0) {
                    this.showIngredients = false;
                }
            },
        });
</script>
