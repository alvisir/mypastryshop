import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.store('apiUrl', import.meta.env.VITE_API_URL_ENDPOINT);

Alpine.start();
